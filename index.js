//making express server
//load expressjs modules into our application and saved it in a variable
const express = require("express");
//app is our server; creates an application that uses express and store it as app
const app = express();

const port = 4000;

//middleware - allows the handle of streaming of data; automatically it will parse
//software provides common services
//when you are doing multiple parsing
//app.use (will run all app under js)

	app.use(express.json())

// mock database

let users = [
	{
		username: "BMadrigal",
		email: "fateReader@gmail.com",
		password: "dontTalkAboutMe"
	},
	{
		username: "Loisa",
		email: "stronggirl@gmail.com",
		password: "pressure"
	}
];

let items = [
	{
		name: "roses",
		price: 170,
		isActive: true
	},
	{
		name: "tulips",
		price: 250,
		isActive: true
	}
];


//http method
 // app.get(<endpoint>, <function for req and res>)

 	app.get('/', (req, res) => {

 		res.send('Hello from my first expressJS API')
 		//res.status(200).send('Hello')
 	});

 	app.get('/greeting', (req, res) => {
 		res.send('Hello from Batch182-Marcial')
 	});

 	//retrieval of the mock database

 	app.get('/users', (req, res) => {
 		res.send(users);
 		//alternative way for specificity
 		//res.json(users);
 	});

 	//post method
 	app.post('/users', (req, res) => {

 		console.log(req.body);

 		let newUser = {
 			username : req.body.username,
 			email: req.body.email,
 			password: req.body.password
 		}

 		//inserting newusers into array
 		users.push(newUser);
 		console.log(users);

 		//send the updated users array in the client
 		res.send(users);
 	});

	// Delete method
 	app.delete('/users', (req, res) => {

 		//array method pop() last item will be remove
 		users.pop();
 		console.log(users);

 		//send the new array
 		res.send(users);
 	});

 	//Put method
 	//update user password
 	// index-wild card, specify what data or user you'll receive, 
 	// localhost:4000/users/0
 	app.put('/users/:index', (req, res) => {


 		console.log(req.body)

 		//object contains the value of url params
 		console.log(req.params)

 		// parseInt value of the number coming from request params
 		// ['0'] turns into [0]

 		let index = parseInt(req.params.index);
 		users[index].password = req.body.password;
 		// users[index].username = req.body.username;

 		res.send(users[index]);
 	});

 	//mini act solution

 	app.put('/users/update/:index', (req, res) => {
 		let index = parseInt(req.params.index);
 		users[index].username = req.body.username;
 			res.send(users[index]);
 	});

 	//get a single user -> retriving a single profile

 	app.get('/users/getSingleUser/:index', (req, res) => {
 		console.log(req.params);

 		//let req.params = {index: '2'}

 		let index = parseInt(req.params.index)
 		console.log(index); // result: 2

 		//index value should be a number

 		res.send(users[index]);
 		console.log(users[index]);
 	});

// *** A C T I V I T Y - SOLUTION
//1.
 app.get('/items', (req, res) => {
 	res.send(items)
 })

 //2.
app.post('/items', (req, res) => {
	let newItem = 
	{
		name: req.body.name,
		price: req.body.price,
		isActive: req.body.isActive
	}

	items.push(newItem);
	console.log(items);

	res.send(items);
});

//3
app.put('/items/updatePrice/:index', (req, res) => {
	let index = parseInt(req.params.index);
 		items[index].price = req.body.price;
 			res.send(items[index]);
 			console.log(items);
});

//ACTIVITY 2

//1
app.get('/items/getSingleItem/:index', (req, res) => {

	let index = parseInt(req.params.index)
 		console.log(index);

 		res.send(items[index]);
 		console.log(users[index]);
});


//2
app.put('/items/archive/:index', (req, res) => {
	let index = parseInt(req.params.index);
	items[index].isActive = false;
	res.send(items[index]);
	console.log(items);
})

//3

app.put('/items/activate/:index', (req, res) => {
	let index = parseInt(req.params.index);
	items[index].isActive = true;
	res.send(items[index]);
	console.log(items);
})







//port listener w 2 arguments
	app.listen(port, () => console.log(`Server is running at port ${port}`))


